//[SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/tasks');

//[SECTION] Routing Component
	const route = express.Router();

//[SECTION] Tasks Routes
	route.post('/', (req, res) => {
		//execute the createTask() from the controller.
		let taskInfo = req.body;
		controller.createTask(taskInfo).then(result=>
			res.send(result)
			)
	});

	//Retrieve All tasks
	route.get('/', (req, res)=> {
		controller.getAllTasks().then(result => {
			res.send(result);
		})
	})

	//Retrieve single task
	route.get('/:id', (req,res)=>{
		let taskInfo = req.params.id;
		controller.getTask(taskInfo).then(outcome=>
			res.send(outcome))
	});

	//Delete task
	route.delete('/:id', (req, res)=>{
			let taskInfo = req.params.id;
		controller.deleteTask(taskInfo).then(outcome=>{
			res.send(outcome);
		})
	});

	//Update task
	route.put('/:id', (req, res)=>{
		//get the input details from the client.
		let task = req.params.id;
		let katawan = req.body
		controller.updateTask(task, katawan).then(outcome=>{
			res.send(outcome);
		})
	})

//[SECTION] Expose Route System
	module.exports = route;