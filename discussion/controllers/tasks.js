//[SECTION] Dependencies and Modules
const Task = require('../models/Task')

//[SECTION] Functionalities 

	//Create New Task
	module.exports.createTask = (clientInput) => {
		let taskName = clientInput.name
		let newTask = new Task({
			name: taskName
		});
		return newTask.save().then((task, error) =>{
			if (error) {
				return 'Saving New Task Failed';
			} else {
				return 'A New Task Was Created';
			}
		})
	}
	//Retrieve All Tasks
	module.exports.getAllTasks = () => {
		return Task.find({}).then(searchResult =>{
			return searchResult;
		})
	}

	//Retrieve Single Task
	module.exports.getTask = (data)=>{
		return Task.findById(data).then(result => {
			return result;
		})
	};

	//Delete Task
	module.exports.deleteTask = (taskInfo) => {
		return Task.findByIdAndRemove(taskInfo).then((removedTask, err) => {
			if (removedTask) {	
				return `Task Deleted Successfully: ${removedTask}`; 
			} else {
				return 'No Task were Removed!';
			}
		})
	}
	
	//Update Task
	module.exports.updateTask = (taskInfo, newTask) => 
	{
		let status = newTask.status;		
		return Task.findById(taskInfo).then((foundTask, error)=>
		{
		if (foundTask) {
			foundTask.status = status;
			return foundTask.save().then((updatedTask, saveErr)=>{
				if (saveErr) {
					return false;
				} else {
					return updatedTask;
				}
			});
		} else {
			return 'No Task Found'
		}
		});
	}